# Ansible blueprint starter template

Use this project template to [create](https://unfurl.cloud/projects/new#create_from_template) a blueprint that deploys Ansible playbooks.

This project contains the following files, each one contains instructions on how to adapt it to your application:

`README.md`: The readme you are reading right now... feel free to edit this -- it will be displayed on the overview page for this project.

`ensemble-template.yaml`: This contains the blueprint description and metadata and will be included by the ensemble that is created when a user deploys your blueprint.

`service_template.py`: This file contains the core resource type definitions expressed in Python.

`service_template.yaml`: This file contains the same definitions as `service_template.py` but expressed as YAML. If you prefer YAML, edit this file instead of `service_template.py`. It is not included by default in `ensemble-template.yaml` but see the instructions there on how to include this.

`unfurl.yaml`: The presence of this YAML file indicates that this directory contains an Unfurl project.

## Customizing the deployment user interface

If your blueprint declares resource templates have properties that are missing from or user settable, Unfurl Cloud will generate UI so the user can complete and customize their deployment. Properties will be rendered based on their type and whether they are marked as required or not, as well as the "title" and "description" fields in the property's schema. In addition, you can customize how properties displayed by setting the following fields in the property's schema's metadata:

* `user_settable`: If a property is computed or has a default set it will not be settable by the user, set this to true to override.
* `computed`: The opposite of `user_settable` -- forces a property to be treated as computed and hidden from the user.
* `default_unit`: If property type is a TOSCA scalar, for example if the type is `scalar-unit.size` then default_unit could be set to "MB", "GB", "MiB", etc.
* `input_type` Override HTML input control for the property type. In addition to the standard HTML input types "textarea" can be used to create a text area. Also "file" is treated specially, it will render a control that lets the user choose files or directories on Unfurl Cloud.
* `file_types`: If "input_type" is set to 'file', this will limit the files that can be choosen, e.g. `"file_types": [".yaml"]`
* `sensitive`: If true, treat as a password field
* `tab_title`: If set on complex types, the value will appear in a separate tab with the given title.
* `property_metadata`: Applies metadata to individual properties on a complex datatype.
For example, this property declaration applies the `user_settable` metadata key to the environment property on unfurl.datatypes.DockerContainer:

```yaml
      container:
        type: unfurl.datatypes.DockerContainer
        metadata:
          property_metadata:
            environment:
              user_settable: true
```

## Developing a blueprint

You can develop your blueprint locally and have local changes reflected on Unfurl Cloud. Follow the instructions on the `Develop` tab on your blueprint's overview page to use `unfurl` to connect your local development environment to Unfurl Cloud. Once you connect, your local changes will be reflected on Unfurl Cloud when you are in developer mode.

When you are ready to release a new version of your blueprint, tag your blueprint with a [semantic version](https://semver.org/) prefixed with a "v", e.g. `v1.0.0`.

## Help build a free and open cloud!

If you blueprint is public, it will be added to the
[cloudmap](https://github.com/onecommons/cloudmap).
This enables your blueprint to be embedded in other blueprints but more importantly it makes available not just on Unfurl Cloud but by anyone that clones that repository and deploys using any compatible [TOSCA](https://github.com/oasis-open/tosca-community-contributions) orchestrator.

Learn more about our vision of a free and open cloud [here](https://www.unfurl.cloud/blog/the-free-and-open-cloud).

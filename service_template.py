# This file is included by ensemble-template.yaml
# Here we define TOSCA types as Python instead of YAML, see
# https://github.com/onecommons/unfurl/blob/main/tosca-package/README.md

"""This template shows you how to create a blueprint that deploys a Ansible playbook."""
# Edit or replace the example code here.

# unfurl should imported first even if unused
from unfurl.configurators.ansible import AnsibleConfigurator, AnsibleInputs
import tosca
from typing import Any


class ExampleAnsibleManagedResource(tosca.nodes.Root):
    """Define a resource type that uses Ansible."""

    # by defaults attributes that don't start with "_" become TOSCA properties
    example_property: str
    """A property the UI will render for user input"""

    example_attribute: str = tosca.Attribute()  # TOSCA attributes' values are set during deployment

    # these methods are implementations of the TOSCA Standard interface, which provide CRUD operations:

    # the configure operation is invoked when creating and updating a resource
    @tosca.operation(
        # use outputs to map Ansible facts in your playbook to the resource's attributes:
        outputs={"a_fact": "example_attribute"}
    )
    def configure(self, **kw: Any) -> AnsibleConfigurator:
        # run this ansible playbook to create or update the resource
        return AnsibleConfigurator(AnsibleInputs(playbook="playbook-create.yaml"))

    def delete(self, **kw: Any) -> AnsibleConfigurator:
        # run this ansible playbook to delete the resource
        # See https://docs.unfurl.run/configurators.html#ansible
        return AnsibleConfigurator(AnsibleInputs(playbook="playbook-delete.yaml"))

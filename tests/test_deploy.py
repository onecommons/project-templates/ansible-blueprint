from unfurl.testing import isolated_lifecycle, DEFAULT_STEPS_NO_CHECK, assert_no_mypy_errors
from unfurl.localenv import LocalEnv
import os
import pytest

def test_deploy():
    "Create a deployment in a new dashboard using the 'testing' deployment-blueprint" 
    init_args = f"clone {os.path.abspath('.')} --use-deployment-blueprint testing test_deployment".split()
    # this clone this blueprint, deploys it and then tears down the deployment
    list(isolated_lifecycle("", DEFAULT_STEPS_NO_CHECK, init_args=init_args, job_args=["test_deployment"]))

@pytest.mark.skipif(
    not assert_no_mypy_errors, reason="mypy not installed"
)
def test_mypy():
    print("checking", os.path.abspath("service_template.py"))
    # instantiate the ensemble to force the tosca_repositories packages to be installed
    LocalEnv("ensemble-template.yaml").get_manifest(skip_validation=True, safe_mode=True)
    assert_no_mypy_errors("service_template.py")
